# print a String to I/O
# eg: printString(shung): "shung" appears on Run I/O
.macro printString(%str)
	li $v0, 4
	la $a0, %str
	syscall
.end_macro

# print a colon to Run I/O
.macro colon
	li $a0, ':'
	li $v0, 11
	syscall
.end_macro	

# print a newline to Run I/O
.macro newline
	li $a0, '\n'
	li $v0, 11
	syscall
.end_macro	

# format a string by add 2 spaces at the first index and last index
# eg: formatString(shung) turns "shung" into " shung "
	
# copy from str to str2
.macro strcpy(%str, %str2)
	li $t0, 0
loop:
	lb $t1, %str($t0)
	sb $t1, %str2($t0)
	addi $t0, $t0, 1
	bne $t1, $zero, loop
	nop
.end_macro

.macro formatString2(%str)
	strcpy(%str, temp)
	li $t3, ' '
	li $t0, 0
	li $t1, 1
	sb $t3, %str($t0)
loop:
	lb $t2, temp($t0)
	sb $t2, %str($t1)
	addi $t1, $t1, 1
	addi $t0, $t0, 1
	bnez $t2, loop
.end_macro

.macro formatImmReg(%str, %str1, %str2)
	li $t0, 1
	li $t1, 0
	li $t2, 0
while:
	lb $t3, %str($t0)
	addi $t0, $t0, 1
	beq $t3, '(', end_while
	sb $t3, %str1($t1)
	addi $t1, $t1, 1
	
	j while
end_while:
	sb $zero, %str1($t1)
while2:
	lb $t3, %str($t0)
	addi $t0, $t0, 1
	beqz $t3, end_while2
	sb $t3, %str2($t2)
	addi $t2, $t2, 1
	
	j while2
end_while2:
	addi $t2, $t2, -2
	sb $zero, %str2($t2)
.end_macro

.macro formatString(%str) 
	li $t0, 1	# i = 0
	li $t1, 1	# j = 1
	
	strcpy(%str, temp)
	li $t3, ' '
	sb $t3, %str($t0)
	nop
	
while:
	lb $t6, temp($t1)
	beq $t6,$zero, end_while
	nop
	
	sb $t6, %str($t1)
	nop
	addi $t0, $t0, 1
	addi $t1, $t1, 1
	
	j while
	nop
end_while:
	sb $t3, %str($t1)
	sb $zero, %str+1($t1)
.end_macro

# s isIn str?
# true: v0 = 1
# false : v0 = 0
.macro isIn(%s, %str) 
	li $t0, 0		# init t0 = i = 0

parentLoop:			# str loop
	li $v0, 1	
	li $t1, 0 
	move $t2, $t0		# t2 = t0	

childLoop:			# s loop
	lb $a0, %str($t2)	#load str index char
	lb $a1, %s ($t1)	#load s index char
	
	beqz $a0, end_parentLoop	# kill parent loop if read null (in String str)
	beqz $a1, end_childLoop		# kill child loop if read null (in String s)
	bne $a1, $a0 , not_equal	# if ai != a0 goto not_equal
	
	add $t1, $t1, 1			# t1++
	add $t2, $t2, 1			# t2++
	j childLoop	
not_equal:
	li $v0, 0	# v0 = 0	
end_childLoop:
	bnez $v0, exitFunc	# v0 = 1 -> s is in str -> exit 	
	add $t0, $t0, 1		# i++
	j parentLoop	
	
end_parentLoop:
	beq $a1, $a0, exitFunc	 
	li $v0, 0		# v0 = 0
exitFunc:				
.end_macro

# check operand is int register or not
.macro isIntReg(%ope)
	isIn(%ope, INT_REG)
.end_macro
# check operand is floating-point register or not
.macro isFloatReg(%ope)
	isIn(%ope, FLOAT_REG)
.end_macro

.macro isInt(%param)
	li $v0, 1
	li $t0, 1
	lb $a0, %param($t0)
	bne $a0, '-', loop
	add $t0, $t0, 1
loop:
	lb $a0, %param($t0)
	beq $a0, ' ', end_loop
	blt $a0, '0', not_digit
	bgt $a0, '9', not_digit
nextc:	addi $t0, $t0, 1
	j loop
not_digit:
	li $v0, 0
	b exit_func	
end_loop:
	lb $a0, %param + 1 ($t0)
	beqz $a0, exit_func
	li $v0, 0	
exit_func:
.end_macro
.macro isLabel(%ope)
	li $v0, 1
	li $t0, 1
	lb $a0, %ope($t0)
	bgt $a0, 'z', not_lb
	blt $a0, 'A', not_lb
	blt $a0, 'a', A_Z
	b looplb
A_Z:	bgt $a0, 'Z', is_
	b looplb
is_:	bne $a0, '_', not_lb	
	add $t0, $t0, 1
looplb:
	lb $a0, %ope($t0)
	beq $a0, ' ', end_looplb
	blt $a0, '0', not_lb
	bgt $a0, 'z', not_lb
	bge $a0, 'A', a_z1
	bgt $a0, '9', not_lb
a_z1:	blt $a0, 'a', A_Z1
	b nextlb
A_Z1:	bgt $a0, 'Z', is_1
	b nextlb
is_1:	beq $a0, '_', not_lb


nextlb:	addi $t0, $t0, 1
	j looplb
not_lb:
	li $v0, 0	
end_looplb:
	lb $a0, %ope + 1 ($t0)
	beqz $a0, exitFuncl
	li $v0, 0	
exitFuncl:
.end_macro


# check and print %ope
.macro checkAndPrintIntReg(%ope)
	printString(%ope)
	colon
	isIntReg(%ope)
	bnez $v0, valid
	printString(INVALID_OPE)
	newline
	b exit_program
valid:	printString(VALID_OPE)
	newline
.end_macro

.macro checkAndPrintFloatReg(%ope)
	printString(%ope)
	colon
	isFloatReg(%ope)
	bnez $v0, valid
	printString(INVALID_OPE)
	newline
	b exit_program
valid:	printString(VALID_OPE)
	newline
.end_macro

.macro checkAndPrintImmReg(%ope)
	li $v0, 1
	
	printString(%ope)
	colon
	
	formatImmReg(%ope, temp3, temp2)

	formatString2(temp3)
	formatString(temp3)
	
	isInt(temp3)
	bnez $v0, valid1
	nop
	isLabel(temp3)
	bnez $v0, valid1	
	nop
invalid:
	printString(INVALID_OPE)
	newline
	b exit_program
valid1:
	formatString2(temp2)
	formatString(temp2)
	isIntReg(temp2)
	bnez $v0, valid
	
	j invalid
	nop
valid:	printString(VALID_OPE)
	newline
.end_macro

.macro checkAndPrintImm(%ope)
	printString(%ope)
	colon
	isInt(%ope)
	beqz $v0, invalid
	printString(VALID_OPE)
	newline
	b exit
invalid:
	printString(INVALID_OPE)
	newline
	j exitProgram
exit:		
.end_macro 		

.macro checkAndPrintLabel(%ope)
	printString(%ope)
	colon
	isLabel(%ope)
	beqz $v0, invalid
	printString(VALID_OPE)
	b exit
invalid:
	printString(INVALID_OPE)
	j exitProgram
exit:
.end_macro	

.macro checkAndPrintOpc (%FORMAT, %branch)	
	isIn(opc, %FORMAT)
	beqz $v0, exitF
	#newline
	isIn(opc, TWO_CYCLE_OPC)
	beqz $v0, mot_chu_ky
	printString(OPC_1_CC)
	b show_func
mot_chu_ky: 
	printString(OPC_1_CC)
		
show_func:
	printString(opc)
	colon
	printString(VALID_OPC)
	newline
	j %branch
exitF:	
	nop
	
.end_macro

.macro checkAndPrintEmpty(%param)
	lb $t0, %param + 2
	bnez $t0, not_Empty
	b return
not_Empty:
	printString(%param)
	colon
	printString(INVALID_OPE)
	j exitProgram
return:	 
.end_macro

.data
    # Legend:
    # T: Integer Register
    # F: Floating-point Register
    # I: Immediate 
    # A: address
    # L: label    
         
	# 1 "add $t1,$t2,$t3"
	R_FORMAT_TTT: .asciiz  "  add sub addu subu mul and or nor xor sllv srav srlv slt sltu movn movz  "
	
	# 2 "add.s $f0,$f1,$f3"
	R_FORMAT_FFF: .asciiz " add.s sub.s mul.s div.s add.d sub.d mul.d div.d "
	
	# 3 "movn.d $f2,$f4,$t3"
	R_FORMAT_FFT: .asciiz " movn.d movz.d movn.s movz.s"
	
	# 4 "mult $t1,$t2"
	R_FORMAT_TT: .asciiz " mult multu madd maddu msub msubu div divu movf movt jalr clo clz mfc0 mtc0 teq tne tge tgeu tlt tltu "
	
	# 5 "sll $t1,$t2,10"
	R_FORMAT_TTI: .asciiz " sll srl sra movf movt "
	
	# 6 "sqrt.s $f0,$f1" 
	R_FORMAT_FF: .asciiz " sqrt.s floor.w.s ceil.w.s round.w.s trunc.w.s sqrt.d floor.w.d ceil.w.d round.w.d trunc.w.d c.eq.s c.le.s c.lt.s c.eq.d c.le.d c.lt.d abs.s abs.d cvt.d.s cvt.d.w cvt.s.d cvt.s.w cvt.w.d cvt.w.s mov.d movf.d movt.d mov.s movf.s movt.s neg.d neg.s "
	
	# 7 "mfc1 $t1,$f1" 
	R_FORMAT_TF: .asciiz " mfc1 mtc1 "
	
	# 8 "c.eq.s 1,$f0,$f1"
	R_FORMAT_IFF: .asciiz " c.eq.s c.le.s c.lt.s c.eq.d c.le.d c.lt.d "
	
	# 9 "movf.d $f2,$f4,1" 
	R_FORMAT_FFI: .asciiz " movf.d movt.d movf.s movt.s "
	
	# 10 "mfhi $t1" 
	R_FORMAT_T: .asciiz " mfhi mflo mthi mtlo jr jalr "
	
	# 11 "nop" 
	R_FORMAT_0: .asciiz " nop break syscall eret "
	
	# 12 "addi $t1,$t2,-100" 
	I_FORMAT_TTI: .asciiz " addi addiu andi ori xori slti sltiu "
	# 13 "lw $t1,-100($t2)" 
	I_FORMAT_TA: .asciiz " lw ll lwl lwr sw sc swl swr lb lh lhu lbu sb sh "
	
	# 14 "sdc1 $f2,-100($t2)" 
	I_FORMAT_FA: .asciiz " lwc1 ldc1 swc1 sdc1  "
	
	# 15 "teqi $t1,-100" 
	I_FORMAT_FI: .asciiz " teqi tnei tgei tgeiu tlti tltiu lui "
	
	# 16 "beq $t1,$t2,label" 
	H_FORMAT_TTL: .asciiz " bne beq "
	
	# 17 "bgez $t1,label" 
	H_FORMAT_TL: .asciiz " bgez bgezal bgtz blez bltz bltzal "
	
	# 18 "bc1t 1,label" 
	H_FORMAT_IL: .asciiz " bc1t bc1f "
	
	# 19 "bc1t label" 
	# "j target"
	H_J_FORMAT_L: .asciiz " bc1t bc1f j jal "
	
	TWO_CYCLE_OPC: .asciiz " lw ll lwl lwr sw sc swl swr lb lh lhu lbu sb sh lwc1 ldc1 swc1 sdc1 beq bne bgez bgezal bgtz blez bltz bltzal bc1t bc1f bc1t bc1f j jal "
	INT_REG: .asciiz " $zero $at $v0 $v1 $a0 $a1 $a2 $a3 $t0 $t1 $t2 $t3 $t4 $t5 $t6 $t7 $t8 $t9 $s0 $s1 $s2 $s3 $s4 $s5 $s6 $s7 $k0 $k1 $gp $sp $fp $ra $0 $1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11 $12 $13 $14 $15 $16 $17 $18 $19 $20 $21 $22 $23 $24 $25 $26 $27 $28 $29 $30 $31 "
	FLOAT_REG: .asciiz " $f0 $f1 $f2 $f3 $f4 $f5 $f6 $f7 $f8 $f9 $f10 $f11 $f12 $f13 $f14 $f15 $f16 $f17 $f18 $f19 $f20 $f21 $f22 $f23 $f24 $f25 $f26 $f27 $f28 $f29 $f30 $f31 "		 
	INPUT_REQUEST: .asciiz "\nEnter Instruction: "
	INVALID_OPC: .asciiz " Invalid Opcode"
	VALID_OPC: .asciiz " Valid Opcode"
	INVALID_OPE: .asciiz " Invalid Operand"
	VALID_OPE: .asciiz " Valid Operand"
	OPC_1_CC: .asciiz "Completed in 1 clock cycle\n"
	OPC_2_CC: .asciiz "Completed in 2 clock cycles\n"
	instruction: .space 100	
	opc: .space 100	
	ope1: .space 100
	ope2: .space 100	
	ope3: .space 100
	temp: .space 100
	temp2: .space 100
	temp3: .space 100

.text
	la $a0, INPUT_REQUEST
	li $v0, 4
	syscall
	
	la $a0, instruction	
	li $a1, 98
	li $v0, 8
	syscall
	
	jal get_all_op
 	nop
 	
	formatString2(opc)
 	formatString(opc)
 	formatString2(ope1)
    	formatString(ope1)
    	formatString2(ope2)
    	formatString(ope2)
    	formatString2(ope3)
    	formatString(ope3)
    	
    	checkAndPrintOpc(R_FORMAT_TTT, OPES_R_FORMAT_TTT)
	checkAndPrintOpc(R_FORMAT_FFF,OPES_R_FORMAT_FFF)
	checkAndPrintOpc(R_FORMAT_FFT,OPES_R_FORMAT_FFT)
	checkAndPrintOpc(R_FORMAT_TT,OPES_R_FORMAT_TT)
	checkAndPrintOpc(R_FORMAT_TTI,OPES_R_FORMAT_TTI)
	checkAndPrintOpc(R_FORMAT_FF,OPES_R_FORMAT_FF)
	checkAndPrintOpc(R_FORMAT_TF,OPES_R_FORMAT_TF)
	checkAndPrintOpc(R_FORMAT_IFF,OPES_R_FORMAT_IFF)
	checkAndPrintOpc(R_FORMAT_FFI,OPES_R_FORMAT_FFI)
	checkAndPrintOpc(R_FORMAT_T,OPES_R_FORMAT_T)
	checkAndPrintOpc(R_FORMAT_0,OPES_R_FORMAT_0)
	checkAndPrintOpc(I_FORMAT_TTI,OPES_I_FORMAT_TTI)
	checkAndPrintOpc(I_FORMAT_TA,OPES_I_FORMAT_TA)
	checkAndPrintOpc(I_FORMAT_FA,OPES_I_FORMAT_FA)
	checkAndPrintOpc(I_FORMAT_FI,OPES_I_FORMAT_FI)
	checkAndPrintOpc(H_FORMAT_TTL,OPES_H_FORMAT_TTL)
	checkAndPrintOpc(H_FORMAT_TL,OPES_H_FORMAT_TL)
	checkAndPrintOpc(H_FORMAT_IL,OPES_H_FORMAT_IL)
	checkAndPrintOpc(H_J_FORMAT_L,OPES_H_J_FORMAT_L)
	
	newline
	printString(INVALID_OPC)
	b exitProgram

OPES_R_FORMAT_TTT:
	checkAndPrintIntReg(ope1)
	checkAndPrintIntReg(ope2)
	checkAndPrintIntReg(ope3)
	j exitProgram
OPES_R_FORMAT_FFF:
	checkAndPrintFloatReg(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintFloatReg(ope3)
	j exitProgram
OPES_R_FORMAT_FFT:
	checkAndPrintFloatReg(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintIntReg(ope3)
	j exitProgram
OPES_R_FORMAT_TT:
	checkAndPrintIntReg(ope1)	
	checkAndPrintIntReg(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_R_FORMAT_TTI:
	checkAndPrintIntReg(ope1)	
	checkAndPrintIntReg(ope2)
	checkAndPrintImm(ope3)
	j exitProgram
OPES_R_FORMAT_FF:
	checkAndPrintFloatReg(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_R_FORMAT_TF:
	checkAndPrintIntReg(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_R_FORMAT_IFF:
	checkAndPrintImm(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintFloatReg(ope3)
	j exitProgram
OPES_R_FORMAT_FFI:
	checkAndPrintFloatReg(ope1)	
	checkAndPrintFloatReg(ope2)
	checkAndPrintImm(ope3)
	j exitProgram	
OPES_R_FORMAT_T:
	checkAndPrintIntReg(ope1)	
	checkAndPrintEmpty(ope2)
	j exitProgram
OPES_R_FORMAT_0:
	checkAndPrintEmpty(ope1)
	j exitProgram
OPES_I_FORMAT_TTI:
	checkAndPrintIntReg(ope1)	
	checkAndPrintIntReg(ope2)
	checkAndPrintImm(ope3)
	j exitProgram
OPES_I_FORMAT_TA:
	checkAndPrintIntReg(ope1)
	checkAndPrintImmReg(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram 
OPES_I_FORMAT_FA:
	checkAndPrintFloatReg(ope1)
	checkAndPrintImmReg(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_I_FORMAT_FI:
	checkAndPrintFloatReg(ope1)	
	checkAndPrintImm(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_H_FORMAT_TTL:
	checkAndPrintIntReg(ope1)	
	checkAndPrintIntReg(ope2)
	checkAndPrintLabel(ope3)
	j exitProgram
OPES_H_FORMAT_TL:
	checkAndPrintIntReg(ope1)	
	checkAndPrintLabel(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_H_FORMAT_IL:
	checkAndPrintImm(ope1)	
	checkAndPrintLabel(ope2)
	checkAndPrintEmpty(ope3)
	j exitProgram
OPES_H_J_FORMAT_L:
	checkAndPrintLabel(ope1)
	checkAndPrintEmpty(ope2)	
exitProgram:
	li $v0, 10
	syscall
	j exit_program

# get opcode and all
get_all_op: 
	li $t0, 0	# t0 is instruction's index	
	li $t2, 0	# t2 is current index

loop_ignore_space:
	lb $t1, instruction($t0)
	add $t0, $t0, 1
	beq $t1, ' ', loop_ignore_space
	bne $t1, ' ', end_loop_ignore_space
end_loop_ignore_space:
	add $t0, $t0, -1
	
loop_opc:
 	lb $t1, instruction($t0) 	# read instruction
	beqz $t1, end_loop_opc		# end if read null
	beq $t1, ' ', end_loop_opc	# end if read space
	beq $t1, '\n', end_loop_opc	# end if read newline 
	sb $t1, opc ($t2)		# save to opc
	
	add $t0, $t0, 1
	addi $t2, $t2, 1
	j loop_opc
	nop
 end_loop_opc:
	sb $zero, opc + 1 ($t2)		# add null to last index
	
	li $t2, 0
	
loop_ignore_space2:
	lb $t1, instruction($t0)
	add $t0, $t0, 1
	
	bne $t1, ' ', end_loop_ignore_space2
	beq $t1, ' ', loop_ignore_space2
end_loop_ignore_space2:
	add $t0, $t0, -1

 loop_ope1:	
 	lb $t1, instruction($t0) 	
	beqz $t1, end_loop_ope1
	beq $t1, ',', end_loop_ope1	# end if read ','
	beq $t1, '\n', end_loop_ope1
	sb $t1, ope1 ($t2)	
	
	addi $t0, $t0, 1	# t0++, t2++
	addi $t2, $t2, 1
	j loop_ope1
	nop
 end_loop_ope1:
	sb $zero, ope1 + 1 ($t2)	
	
	addi $t0, $t0, 1
	li $t2, 0
	
loop_ignore_space3:
	lb $t1, instruction($t0)
	add $t0, $t0, 1
	
	bne $t1, ' ', end_loop_ignore_space3
	beq $t1, ' ', loop_ignore_space3
end_loop_ignore_space3:
	add $t0, $t0, -1

 loop_ope2:	
 	lb $t1, instruction($t0)
	beqz $t1, end_loop_ope2	
	beq $t1, ',', end_loop_ope2	
	beq $t1, '\n', end_loop_ope2	
	sb $t1, ope2 ($t2)	
	
	addi $t2, $t2, 1
	addi $t0, $t0, 1	
	j loop_ope2
 end_loop_ope2:
	sb $zero, ope1 + 1 ($t2)	
	
	addi $t0, $t0, 1
	li $t2, 0
	
loop_ignore_space4:
	lb $t1, instruction($t0)
	add $t0, $t0, 1
	
	bne $t1, ' ', end_loop_ignore_space4
	beq $t1, ' ', loop_ignore_space4
end_loop_ignore_space4:
	add $t0, $t0, -1

 loop_ope3:	
 	lb $t1, instruction($t0) 	
	beqz $t1, end_loop_ope3	
	beq $t1, ' ', end_loop_ope3	# end if read space
	beq $t1, '\n', end_loop_ope3	
	sb $t1, ope3 ($t2)	
	
	addi $t2, $t2, 1
	addi $t0, $t0, 1	
	j loop_ope3
	nop
 end_loop_ope3:
	sb $zero, ope3 + 1 ($t2)
	
	jr $ra
exit_program:
