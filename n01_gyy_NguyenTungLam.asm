#============HEXA-KEYBOARD===================
# Key value
	.eqv KEY_0 0x11
	.eqv KEY_1 0x21
	.eqv KEY_2 0x41
	.eqv KEY_3 0x81
	.eqv KEY_4 0x12
	.eqv KEY_5 0x22
	.eqv KEY_6 0x42
	.eqv KEY_7 0x82
	.eqv KEY_8 0x14
	.eqv KEY_9 0x24
	.eqv KEY_a 0x44
	.eqv KEY_b 0x84
	.eqv KEY_c 0x18
	.eqv KEY_d 0x28
	.eqv KEY_e 0x48
	.eqv KEY_f 0x88
	
	.eqv IN_ADDRESS_HEXA_KEYBOARD 0xFFFF0012
	.eqv OUT_ADDRESS_HEXA_KEYBOARD 0xFFFF0014
	
#=============================================

#=============KEYBOARD========================
	
	.eqv KEY_READY 0xFFFF0000
	.eqv KEY_CODE 0xFFFF0004
	
#=============================================

#=============MARSBOT=========================

.eqv HEADING 0xffff8010 # Integer: An angle between 0 and 359
 			# 0 : North (up)
 			# 90: East (right)
			# 180: South (down)
			# 270: West (left)
.eqv MOVING 0xffff8050 # Boolean: whether or not to move
.eqv LEAVETRACK 0xffff8020 # Boolean (0 or non-0):
 			# whether or not to leave a track
.eqv WHEREX 0xffff8030 # Integer: Current x-location of MarsBot
.eqv WHEREY 0xffff8040 # Integer: Current y-location of MarsBot

#=============================================
	
.data
	input_code: .space 50
	input_length: .word 0
	path: .space 1000
	length_path: .word 0
	now_heading: .word 0
	error_message: .asciiz "Wrong control code!"
	test_message: .asciiz "Hello"
	move_message: .asciiz "Move"
	stop_message: .asciiz "Stop"
	goleft_message: .asciiz "Go Left"
	goright_message: .asciiz "Go Right"
	track_message: .asciiz "Leave Track"
	untrack_message: .asciiz "Untrack"
	goback_message: .asciiz "Go Back"
	
	
	# Code
	MOVE_CODE: .asciiz "1b4"
	STOP_CODE: .asciiz "c68"
	GOLEFT_CODE: .asciiz "444"
	GORIGHT_CODE: .asciiz "666"
	TRACK_CODE: .asciiz "dad"
	UNTRACK_CODE: .asciiz "cbc"
	GOBACK_CODE: .asciiz "999"
.text

#---------------------------------------------------------
# Enable interrupt for Hexa-Keyboard

	li $t1, IN_ADDRESS_HEXA_KEYBOARD
	li $t2, 0x80
	sb $t2, 0($t1) # bit 7 of = 1 to enable interrupt
#---------------------------------------------------------

main:
	li $at, HEADING
	sw $zero, 0($at)
	la $k0, KEY_READY
	li $k1, KEY_CODE 
	
	j execute_goright
	nop
loop:

wait_key:
	lw $t5, 0($k0)			#$t5 = [$k1] = KEY_READY
	beq $t5, $zero, wait_key	#if $t5 == 0 then Polling 
	nop
	beq $t5, $zero, wait_key
read_key:
	lw $t6, 0($k1)			#$t6 = [$k0] = KEY_CODE
	beq $t6, 127 , continue		#if $t6 == delete key then remove input
						#127 is delete key in ascii
		
	bne $t6, '\n' , loop		#if $t6 != '\n' then Polling
	nop
	bne $t6, '\n' , loop
	
print_code:
	li $v0, 4
	la $a0, input_code
	syscall
	
check_code:

check_length:
	la $s1, input_length
	lw $s1, 0($s1)
	addi $t0, $zero, 3
	bne $s1, $t0, output_error
	nop
	
# s0 input code
# s1 designed code
# t0 status
	la $s0, input_code
	add $t0, $zero, $zero
check_move_code:
	la $s1, MOVE_CODE
	jal is_equal_code
	bne $t0, $zero, execute_move
	nop
check_stop_code:
	la $s1, STOP_CODE
	jal is_equal_code
	bne $t0, $zero, execute_stop
	nop
check_goleft_code:
	la $s1, GOLEFT_CODE
	jal is_equal_code
	bne $t0, $zero, execute_goleft
	nop
check_goright_code:
	la $s1, GORIGHT_CODE
	jal is_equal_code
	bne $t0, $zero, execute_goright
	nop
check_track_code:
	la $s1, TRACK_CODE
	jal is_equal_code
	bne $t0, $zero, execute_track
	nop
check_untrack_code:
	la $s1, UNTRACK_CODE
	jal is_equal_code
	bne $t0, $zero, execute_untrack
	nop
check_goback_code:
	la $s1, GOBACK_CODE
	jal is_equal_code
	bne $t0, $zero, execute_goback
	nop

output_error:
	li $v0, 55
	la $a0, error_message
	syscall
	
continue:
	jal clear_input
	nop
	j loop
	nop
	
	
# Check code string
is_equal_code:
	add $t1, $zero, $zero
	li $t4, 3
	la $s2, input_code
loop_check_char: 
	# loop through 3 char
	# s2 input code (real address)
	# s3 designed code
	# t1 index
	
	beq $t1, $t4, is_equal
	nop
	add $s2, $s0, $t1 # get real address of input char
	add $s3, $s1, $t1 # get real address of code
	lb $s2, 0($s2)
	lb $s3, 0($s3)
	bne $s2, $s3, is_not_equal
	nop
	addi $t1, $t1, 1
	j loop_check_char
	nop

is_equal:
	li $t0, 1
	jr $ra
	nop
is_not_equal:	
	add $t0, $zero, $zero
	jr $ra
	nop
	
	
# Clear input

clear_input:
	#back up s0, s1, t0, t1
	addi $sp,$sp,4
	sw $s0, 0($sp)
	addi $sp,$sp,4
	sw $s1, 0($sp)
	addi $sp,$sp,4
	sw $t0, 0($sp)
	addi $sp,$sp,4
	sw $t1, 0($sp)
	
process:
	la $s0, input_code
	la $s1, input_length
	lw $s1, 0($s1)
	add $t0, $zero, $zero	#index
	add $t1, $zero, $zero	#real address t1 = s0 + t0
	
loop_clear_input:
	beq $t0, $s1, end_loop_clear
	nop
	add $t1, $s0, $t0
	sb $zero, 0($t1)
	addi $t0, $t0, 1
	j loop_clear_input
	nop
	

end_loop_clear:
	#set length = 0
	la $s1, input_length
	sw $zero, 0($s1)
		
	#restore 
	sw $t1, 0($sp)
	addi $sp,$sp,-4
	
	sw $t0, 0($sp)
	addi $sp,$sp,-4
	
	sw $s1, 0($sp)
	addi $sp,$sp,-4
	
	sw $s0, 0($sp)
	addi $sp,$sp,-4
	
	jr $ra
	nop
	
# Execute

execute_move:	
	#backup
	addi $sp,$sp,4
	sw $at,0($sp)
	addi $sp,$sp,4
	sw $k0,0($sp)
	#processing	
	li $at, MOVING 
 	addi $k0, $zero,1 
	sb $k0, 0($at) 	
	#restore
	lw $k0, 0($sp)
	addi $sp,$sp,-4
	lw $at, 0($sp)
	addi $sp,$sp,-4
	
	j continue
	nop
	
execute_stop:
	#backup
	addi $sp,$sp,4
	sw $at,0($sp)
	addi $sp,$sp,4
	sw $k0,0($sp)
	#processing	
	li $at, MOVING 
 	add $k0, $zero, $zero
	sb $k0, 0($at) 	
	#restore
	lw $k0, 0($sp)
	addi $sp,$sp,-4
	lw $at, 0($sp)
	addi $sp,$sp,-4
	
	j continue
	nop
	
execute_goleft:
	la $t1, now_heading
	lw $t3, 0($t1)	
	addi $t3, $t3, -90
 	sw $t3, 0($t1)
 	
 	# store status 
 	jal store_path
	nop
	
	li $t1, HEADING
	sw $t3, 0($t1)
	
	j continue
	nop
	
execute_goright:
	la $t1, now_heading
	lw $t3, 0($t1)
	addi $t3, $t3, 90
 	sw $t3, 0($t1)
 	
 	# store status after turning
 	jal store_path
	nop
	
	li $t1, HEADING
	sw $t3, 0($t1)
	
	j continue
	nop
	
execute_track:
	li $v0, 55
	la $a0, track_message
	syscall
	
	#backup
	addi $sp,$sp,4
	sw $at,0($sp)
	addi $sp,$sp,4
	sw $k0,0($sp)
	#processing
	li $at, LEAVETRACK # change LEAVETRACK port
	addi $k0, $zero,1 # to logic 1,
 	sb $k0, 0($at) # to start tracking
 	#restore
	lw $k0, 0($sp)
	addi $sp,$sp,-4
	lw $at, 0($sp)
	addi $sp,$sp,-4
	
	j continue
	nop
	
execute_untrack:
	#backup
	addi $sp,$sp,4
	sw $at,0($sp)
	addi $sp,$sp,4
	sw $k0,0($sp)
	#processing
	li $at, LEAVETRACK # change LEAVETRACK port
	add $k0, $zero, $zero # to logic 1,
 	sb $k0, 0($at) # to start tracking
 	#restore
	lw $k0, 0($sp)
	addi $sp,$sp,-4
	lw $at, 0($sp)
	addi $sp,$sp,-4
	
	j continue
	nop
	
execute_goback:
	la $s7, path
	la $s5, length_path
	lw $s5, 0($s5)
	add $s7, $s7, $s5
begin:	addi $s5, $s5, -12 	# go back 1 struct
	
	addi $s7, $s7, -12	
	lw $s6, 8($s7)		# last heading
	addi $s6, $s6, 180	# reverse heading
	
	la $t8, now_heading	# turn marsbot
	sw $s6, 0($t8)
	li $t8, HEADING
	sw $s6, 0($t8)

go_to_first_point_of_edge:	
	lw $t9, 0($s7)		# x-location of the starting point of the edge
	li $t8, WHEREX		# current x-location
	lw $t8, 0($t8)

	bne $t8, $t9, go_to_first_point_of_edge
	
	lw $t9, 4($s7)		# y-location of the starting point of the edge
	li $t8, WHEREY		# current y-location
	lw $t8, 0($t8)
	
	bne $t8, $t9, go_to_first_point_of_edge
	
	beq $s5, 0, finish	# reach point
		
	j begin
	nop
	
finish:	
	li $t8, MOVING		# make marsbot stop
	sw $zero, 0($t8)
	la $t8, now_heading
	add $s6, $zero, $zero
	sw $s6, 0($t8)		# update heading
	la $t8, length_path
	sw $s5, 0($t8)		# update lengthPath = 0
	li $t8, HEADING
	sw $s6, 0($t8)
	
	j execute_goright
	nop
	
	
store_path:
	# backup t0, t1, s1, s2, s3
	addi $sp, $sp, 4
	sw $t0, 0($sp)
	addi $sp, $sp, 4
	sw $t1, 0($sp)
	addi $sp, $sp, 4
	sw $s1, 0($sp)
	addi $sp, $sp, 4
	sw $s2, 0($sp)
	addi $sp, $sp, 4
	sw $s3, 0($sp)
	
	# process
	li $s1, WHEREX
	lw $s1, 0($s1)
	
	li $s2, WHEREY
	lw $s2, 0($s2)
	
	#li $s3, HEADING
	la $s3, now_heading
	lw $s3, 0($s3)
	
	la $t0, path
	la $t1, length_path
	lw $t1, 0($t1)
	add $t0, $t0, $t1 # address to store next
	
	# store status
	sw $s1, 0($t0)
	sw $s2, 4($t0)
	sw $s3, 8($t0)
	
	# update length
	addi $t1, $t1, 12
	la $t0, length_path
	sw $t1, 0($t0)	
	
	# restore
	lw $s3, 0($sp)
	addi $sp, $sp, -4
	lw $s2, 0($sp)
	addi $sp, $sp, -4
	lw $s1, 0($sp)
	addi $sp, $sp, -4
	lw $t1, 0($sp)
	addi $sp, $sp, -4
	lw $t0, 0($sp)
	addi $sp, $sp, -4
	
	jr $ra
	nop

	

# Interrupt routine for hexa-keyboard
.ktext 0x80000180

# backup reg to stack
	addi $sp,$sp,4
	sw $ra,0($sp)
	addi $sp,$sp,4
	sw $t1,0($sp)
	addi $sp,$sp,4
	sw $t2,0($sp)
	addi $sp,$sp,4
	sw $t3,0($sp)
	addi $sp,$sp,4
	sw $a0,0($sp)
	addi $sp,$sp,4
	sw $at,0($sp)
	addi $sp,$sp,4
	sw $s0,0($sp)
	addi $sp,$sp,4
	sw $s1,0($sp)
	addi $sp,$sp,4
	sw $s2,0($sp)
	addi $sp,$sp,4
	sw $t4,0($sp)
	addi $sp,$sp,4
	sw $s3,0($sp)
	
# get code
	li $t1, IN_ADDRESS_HEXA_KEYBOARD
	li $t2, OUT_ADDRESS_HEXA_KEYBOARD
scan_row1:
	li $t3, 0x81
	sb $t3, 0($t1)
	lbu $a0, 0($t2)
	bnez $a0, get_key_char
	nop
	
scan_row2:
	li $t3, 0x82
	sb $t3, 0($t1)
	lbu $a0, 0($t2)
	bnez $a0, get_key_char
	nop
	
scan_row3:
	li $t3, 0x84
	sb $t3, 0($t1)
	lbu $a0, 0($t2)
	bnez $a0, get_key_char
	nop
	
scan_row4:
	li $t3, 0x88
	sb $t3, 0($t1)
	lbu $a0, 0($t2)
	bnez $a0, get_key_char
	nop
	
get_key_char:
	beq $a0, KEY_0, case_0
	beq $a0, KEY_1, case_1
	beq $a0, KEY_2, case_2
	beq $a0, KEY_3, case_3
	beq $a0, KEY_4, case_4
	beq $a0, KEY_5, case_5
	beq $a0, KEY_6, case_6
	beq $a0, KEY_7, case_7
	beq $a0, KEY_8, case_8
	beq $a0, KEY_9, case_9
	beq $a0, KEY_a, case_a
	beq $a0, KEY_b, case_b
	beq $a0, KEY_c, case_c
	beq $a0, KEY_d, case_d
	beq $a0, KEY_e, case_e
	beq $a0, KEY_f, case_f
	
	#$s0 store code in char type
case_0:	li $s0, '0'
	j store_code
case_1:	li $s0, '1'
	j store_code
case_2:	li $s0, '2'
	j store_code
case_3:	li $s0, '3'
	j store_code
case_4:	li $s0, '4'
	j store_code
case_5:	li $s0, '5'
	j store_code
case_6:	li $s0, '6'
	j store_code
case_7:	li $s0, '7'
	j store_code
case_8:	li $s0, '8'
	j store_code
case_9:	li $s0, '9'
	j store_code
case_a:	li $s0, 'a'
	j store_code
case_b:	li $s0, 'b'
	j store_code
case_c:	li $s0, 'c'
	j store_code
case_d:	li $s0, 'd'
	j store_code
case_e:	li $s0,	'e'
	j store_code
case_f:	li $s0, 'f'
	j store_code
	
store_code:
	la $s1, input_code
	la $s2, input_length
	lw $s3, 0($s2)
	
	# get loc
	add $s4, $s1, $s3
	# insert new key
	sb $s0, 0($s4)
	# increment length
	addi $s3, $s3, 1
	sw $s3, 0($s2)
	
	
# restore reg
restore:
	lw $s3, 0($sp)
	addi $sp,$sp,-4
	lw $t4, 0($sp)
	addi $sp,$sp,-4
	lw $s2, 0($sp)
	addi $sp,$sp,-4
	lw $s1, 0($sp)
	addi $sp,$sp,-4
	lw $s0, 0($sp)
	addi $sp,$sp,-4
	lw $at, 0($sp)
	addi $sp,$sp,-4
	lw $a0, 0($sp)
	addi $sp,$sp,-4
	lw $t3, 0($sp)
	addi $sp,$sp,-4
	lw $t2, 0($sp)
	addi $sp,$sp,-4
	lw $t1, 0($sp)
	addi $sp,$sp,-4
	lw $ra, 0($sp)
	addi $sp,$sp,-4

# return normal routine
# Evaluate the return address of main routine
# epc <= epc + 4
#--------------------------------------------------------
next_pc:
	mfc0 $at, $14 # $at <= Coproc0.$14 = Coproc0.epc
	addi $at, $at, 4 # $at = $at + 4 (next instruction)
	mtc0 $at, $14 # Coproc0.$14 = Coproc0.epc <= $at
return: eret # Return from exception
	
	
